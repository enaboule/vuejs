import Vue from 'vue'

import Router from 'vue-router'

// const menu            = () => import('@/components/menu/menu'),
//       sommaire        = () => import('@/containers/sommaire/sommaire'),
//       accueil         = () => import('@/containers/accueil/accueil'),
//       player          = () => import('@/components/player/player'),
//       intro           = () => import('@/components/intro/intro'),
//       categories      = () => import('@/components/categories/categories'),
//       partenairescomp = () => import('@/components/partenaires/partenaires'),
//       partenaires     = () => import('@/containers/partenaires/partenaires'),
//       listes          = () => import('@/components/listes/listes'),
//       profile         = () => import('@/components/profile/profile')

// const menu            = r => require.ensure([], () => r(require('@/components/menu/menu')), 'first'),
//       sommaire        = r => require.ensure([], () => r(require('@/containers/sommaire/sommaire')), 'first'),
//       accueil         = r => require.ensure([], () => r(require('@/containers/accueil/accueil')), 'first'),
//       player          = r => require.ensure([], () => r(require('@/components/player/player'))),
//       intro           = r => require.ensure([], () => r(require('@/components/intro/intro')), 'first'),
//       categories      = r => require.ensure([], () => r(require('@/components/categories/categories')), 'first'),
//       partenairescomp = r => require.ensure([], () => r(require('@/components/partenaires/partenaires'))),
//       partenaires     = r => require.ensure([], () => r(require('@/containers/partenaires/partenaires'))),
//       listes          = r => require.ensure([], () => r(require('@/components/listes/listes'))),
//       profile         = r => require.ensure([], () => r(require('@/components/profile/profile')))

import menu from '@/components/menu/menu'
import sommaire from '@/containers/sommaire/sommaire'
import pages from '@/containers/pages/pages'
import page from '@/components/page/page'
import player from '@/components/player/player'
import intro from '@/components/intro/intro'
import categories from '@/components/categories/categories'
import listes from '@/components/listes/listes'
import profile from '@/components/profile/profile'

Vue.use(Router)

export default new Router({ routes:

  [{

      path: '*',

      redirect: '/'

    },{

      path: '/',

      component: intro,

      children: [{

          path: '/',

          name: 'intro',

          components: { default: intro }

        }]

    },{

      path: '/sommaire',

      name: 'lolilol',

      components: { default: sommaire },

      children:[{

          path: '/sommaire/',

          name: 'sommaire',

          components: { default: categories, menu: menu }

        },{

          path: '/sommaire/:category(raconter|guider|conseiller)',

          name: ':category',

          components: { default: listes, menu: menu },

        },{

          path: '/sommaire/:category(raconter|guider|conseiller)/:profile',

          props: true,

          meta: { requiresAuth: true },

          name: ':profile',

          components: { default: profile, menu: menu },

        }]

    },{

          path: '/:pages(partenaires|apropos|mentionslegales)',

          components: { default: pages },

          children: [{

            path: '/:page(partenaires|apropos|mentionslegales)',

            name: ':page',

            components: { default: page, menu: menu },

          }]

          }

      ] } )
