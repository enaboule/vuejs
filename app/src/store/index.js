import Vue from 'vue'

import Vuex from 'vuex'

import { profiles }  from "@/constants/profiles.json"

import fiches  from "@/constants/fiches/"

Vue.use(Vuex)

const state = {

  profiles,

  fiches,

  'loading' : false,

  listesBuffer: false,

  stopVideo: false,

  loadingContent : {

    titre: false,

    titreNext: false,

    titreNextDraw: false,

    categories : false,

    listes: false,

    profiles: false,

    profile: false

  },

  preLoadingScreen : {

    test: false,

    intro: false,

    introNext : false,

    sommaire: false,

    listes : {

      bool : false,

      category: ''

    },
    profile: {

      bool: false,

      category: '',

      slug: ''

    }

  },

  loadingScreen: {

    intro: false,

    introNext: false,

    sommaire: false,

    listes: {

      bool: false,

      category: ''

    },

    profile: false,

    preLoad: false,

    reload: false

  },

  videos: {

    intro: false,

    profile : false

  }

}

const mutations = {

  SET_LISTES_BUFFER: (state, set) => { state[set.screen] = set.bool },

  SET_STOP_VIDEO: (state, set) => { state[set.screen] = set.bool },

  SET_LOADING_STATE: (state, set) => { state.loadingScreen[set.screen] = set.bool },

  SET_PRELOADING_STATE: (state, set) => { state.preLoadingScreen[set.screen] = set.bool },

  SET_LOADING_CONTENT: (state, set) => { state.loadingContent[set.content] = set.bool },

  SET_SRC_VIDEO: (state, set) => { state.videos[set.screen] = set.bool },

  SET_PRELOADING_STATE_LISTES: (state, set) => {

    state.preLoadingScreen[set.screen].bool = set.bool

    state.preLoadingScreen[set.screen].category = set.category

  },

  SET_PRELOADING_STATE_PROFILE: (state, set) => {

    state.preLoadingScreen[set.screen].bool = set.bool

    state.preLoadingScreen[set.screen].category = set.category

    state.preLoadingScreen[set.screen].slug = set.slug

  }

 }

const getters = {

  getStopVideo: (state) => (screen) => {return state[screen] },

  getListesBuffer: (state) => (content) => {return state[content] },

  getLoadingContent: (state) => (content) => {return state.loadingContent[content] },

  getLoadingScreen:  (state) => (screen) => { return state.loadingScreen[screen] },

  getPreLoadingScreen:  (state) => (screen) => { return state.preLoadingScreen[screen] },

  getPreLoadingScreenListes:  (state) => (screen) => {

    let listes = {

      'bool': state.preLoadingScreen[screen].bool,

      'category': state.preLoadingScreen[screen].category
    }

    return listes

  },

  getPreloadingScreenProfile: (state) => (screen) => {

    let profile = {

      'bool': state.preLoadingScreen[screen].bool,

      'category': state.preLoadingScreen[screen].category,

      'slug': state.preLoadingScreen[screen].slug

    }

    return profile

  },

  getPreLoadingCategory:  (state) => (screen) => { return state.preLoadingScreen[screen] },

  getSrcVideos: (state) => (screen) => { return state.videos[screen] },

  profiles: state => state.profiles,

  searchProfile: function (state) {

    var self = this

    return function (args) {

      console.log("ARGS PROFILE || =>", args)

      console.log("PROFILE STATE || =>", state.profiles)

      let category = args.category

      let profile = args.profile

      let res = state.profiles[category].hasOwnProperty(profile) ? state.profiles[category][profile] : null

      return res

    }

  },

  searchCategory: function (state) {

    var self = this

    return function (args) {

      let category = args.category

      let res = state.profiles[category] ? state.profiles[category] : null

      return res

    }

  },

  searchFiche: function (state) {

    var self = this

    return function (args) {

      console.log("ARGS FICHE || =>", args)

      let id = args

      let res = state.fiches[id] ? state.fiches[id] : null

      return res.fiche

    }

  }

}

let store = new Vuex.Store({ state, mutations, getters })

export default store
