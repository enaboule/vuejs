const pathFile = (file, folder) => {

  try {

    return require("@/assets/medias/" + folder + "/" + file)

  } catch (e) {

    console.log(e)

  }

}

const pathProfile = (slug, category) => {

  let res = 'sommaire' + '/' + category + '/' + slug

  res.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '')

  return res

}

const pathProfiles = (slug, category) => {

  let res = category + '/' + slug

  res.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '')

  return res

}

const selectColor = (category) => {

  if (category === "raconter") { var color = "yellow" }

  else if (category === "guider") { var color = "blue" }

  else if (category === "conseiller") { var color = "red" }

  return color

}

const selectStyle = (category, type) => {

  if (category === "raconter")

    var color = type === "button" ? "jauneBtn" : "jaune"

  else if (category === "guider")

    var color = type === "button" ? "bleuBtn" : "bleu"

  else if (category === "conseiller")

    var color =  type === "button" ? "rougeBtn" : "rouge"

  return color

}

const countTitre = (obj, toFind) => {

  var count = 0

  for (var key in obj) { if (obj.hasOwnProperty(key)) { if(obj[key].hasOwnProperty(toFind)) ++count }

  }

  return count

}

const oneTimeEvent = (node, type, callback) => {

  var self = this

  node.addEventListener(type, function(e) {

    e.target.removeEventListener(e.type, arguments[0].callee);

    return (self[callback](e))

  })

}


export {

  pathProfile,

  pathProfiles,

  pathFile,

  selectStyle,

  selectColor,

  countTitre,

}
