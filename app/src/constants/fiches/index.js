const brunoGalland               = require('@/constants/fiches/employeur/brunoGalland.js')

const claireMoyrand              = require('@/constants/fiches/employeur/claireMoyrand.js')

const clemencePronon             = require('@/constants/fiches/employeur/clemencePronon.js')

const genevieveMichelin          = require('@/constants/fiches/employeur/genevieveMichelin.js')

const isabelleDoreRive           = require('@/constants/fiches/employeur/isabelleDoreRive.js')

const laurenceKhamkham           = require('@/constants/fiches/employeur/laurenceKhamkham.js')

const xavierdelaSelle            = require('@/constants/fiches/employeur/xavierdelaSelle.js')

const yvanPerreton               = require('@/constants/fiches/employeur/yvanPerreton.js')

const bernadetteCabouretLaurioux = require('@/constants/fiches/enseignants/bernadetteCabouretLaurioux.js')

const dominiqueValerian          = require('@/constants/fiches/enseignants/dominiqueValerian.js')

const jacquelineBayon            = require('@/constants/fiches/enseignants/jacquelineBayon.js')

const marianneThivend            = require('@/constants/fiches/enseignants/marianneThivend.js')

const michelDepeyre              = require('@/constants/fiches/enseignants/michelDepeyre.js')

const stephaneFrioux             = require('@/constants/fiches/enseignants/stephaneFrioux.js')

const xavierHelary               = require('@/constants/fiches/enseignants/xavierHelary.js')

const adrienAllier               = require('@/constants/fiches/anciens/adrienAllier.js')

const audreyRoche                = require('@/constants/fiches/anciens/audreyRoche.js')

const claireMontchamp            = require('@/constants/fiches/anciens/claireMontchamp.js')

const jeromeLac                  = require('@/constants/fiches/anciens/jeromeLac.js')

const lauryDugand                = require('@/constants/fiches/anciens/lauryDugand.js')

const lionelLacour               = require('@/constants/fiches/anciens/lionelLacour.js')

const lionelMignot               = require('@/constants/fiches/anciens/lionelMignot.js')

const louisBaldasseroni          = require('@/constants/fiches/anciens/louisBaldasseroni.js')

const nathalieBlanc              = require('@/constants/fiches/anciens/nathalieBlanc.js')

const nicolasAucourt             = require('@/constants/fiches/anciens/nicolasAucourt.js')

const sylvainChomienne           = require('@/constants/fiches/anciens/sylvainChomienne.js')

const thomasLascombe             = require('@/constants/fiches/anciens/thomasLascombe.js')

const tristanVuillet             = require('@/constants/fiches/anciens/tristanVuillet.js')

export default {

  brunoGalland,

  claireMoyrand,

  clemencePronon,

  genevieveMichelin,

  isabelleDoreRive,

  laurenceKhamkham,

  xavierdelaSelle,

  yvanPerreton,

  bernadetteCabouretLaurioux,

  dominiqueValerian,

  jacquelineBayon,

  marianneThivend,

  michelDepeyre,

  stephaneFrioux,

  xavierHelary,

  adrienAllier,

  audreyRoche,

  claireMontchamp,

  jeromeLac,

  lauryDugand,

  lionelLacour,

  lionelMignot,

  louisBaldasseroni,

  nathalieBlanc,

  nicolasAucourt,

  sylvainChomienne,

  thomasLascombe,

  tristanVuillet

}
