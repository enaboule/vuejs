const fiche =
[{
  titre:"Portrait",
  text:`
    <h1 class="profile__content--title bleu">Portrait</h1>
    Professeur des universités à l’Université Jean Moulin Lyon 3.<br>
    Professeur d’Histoire romaine, responsable du master Histoire médiation culturelle et communication, mention patrimoine et musées.`
},{
  titre:"Compétences acquises",
  text:`
    <h1 class="profile__content--title bleu">Compétences acquises au cours des études d'histoire</h1>
    <ul>
      <li>Appliquer les méthodes de recherche et d’investigation</li>
      <li>Être curieux.se</li>
      <li>Être régulier.e dans le travail</li>
      <li>Être autonome</li>
      <li>Être rigoureux.se</li>
      <li>Développer l’esprit critique</li>
    </ul>
    Les référentiels de compétences :
    <ul>
      <li><a class="linkTxt bleu" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a> >Ministère de l’Enseignement supérieur et de l’Innovation > Formations et diplômes > Licence</li>
      <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Le Référentiel de compétences de l’Université Jean Monnet</a></li>
    </ul>`
},{
  titre:"Quelques conseils pour réussir",
  text:`
    <h1 class="profile__content--title bleu">Quelques conseils pour réussir</h1>
    Passion et régularité dans le travail sont indispensables.`
},{
  titre:"Et après…",
  text:`
    <h1 class="profile__content--title bleu">Le devenir des diplômé.e.s</h1>
    - Exemples de secteurs, de métiers ou de poursuites d’études possibles :
    <ul>
      <li>Enseignement</li>
      <li>Patrimoine</li>
      <li>culture</li>
      <li>musées</li>
      <li>Archives</li>
      <li>Archéologie</li>
      <li>Journalisme</li>
      <li>Fonction publique</li>
    </ul>
    - Le devenir des diplômé.e.s :
    <ul>
      <li>Enquêtes sur les poursuites d’études après la licence d’Histoire et sur l’insertion des diplômé.es de master de l’Université Jean Moulin Lyon 3 : <a class="linkTxt bleu" href="http://www.univ-lyon3.fr/fr/insertion-professionnelle/enquetes-d-insertion/les-enquetes-d-insertion-professionnelle-de-l-ofip-926114.kjsp">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt bleu" href="http://enquetedhistoires.fr/#/sommaire/raconter">Témoignages des diplômé.e.s</a></li>
    </ul>`
},{
  titre: `L’offre de formation`,
  text:`
    <h1 class="profile__content--title bleu">L’offre de formation</h1>
    <ul>
      <li><a class="linkTxt bleu" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt bleu" href="http://facdeslettres.univ-lyon3.fr/formations-histoire-787235.kjsp?RH=1396014493150">Département Histoire de l'Université jean Moulin Lyon 3</a></li>
    </ul>`
}]
export  { fiche }
