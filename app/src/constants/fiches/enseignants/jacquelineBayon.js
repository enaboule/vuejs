const fiche =
[{
  titre:"Portrait",
  text:`
    <h1 class="profile__content--title bleu">Portrait</h1>
    Professeur d'Université ( Histoire Moderne/Patrimoine)<br>
    Université Jean Monnet Saint-Étienne. UDL<br>
    Responsable du Master International  Joint Master Degree Dynamics of  Cultural Landscapes and heritage Management ( JMD DYCLAM) et du Master  Histoire Civilisation Patrimoine.<br>
    Responsable du Campus Patrimoine Le Corbusier`
},{
  titre:"Compétences acquises",
  text:`
    <h1 class="profile__content--title bleu">Compétences acquises au cours des études d'histoire</h1>
    <ul>
      <li>Savoir enquêter</li>
      <li>savoir analyser</li>
      <li>Comprendre un dossier</li>
      <li>L’instruire en toute objectivité</li>
      <li>Le présenter et le transmettre</li>
    </ul>
    Les référentiels de compétences :
    <ul>
      <li><a class="linkTxt bleu" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a> >Ministère de l’Enseignement supérieur et de l’Innovation > Formations et diplômes > Licence </li>
      <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Le Référentiel de compétences de l’Université Jean Monnet</a></li>
    </ul>`
},{
  titre:"Quelques conseils pour réussir",
  text:`
    <h1 class="profile__content--title bleu">Quelques conseils pour réussir</h1>
    <ul>
      <li>Avoir une part d’éthique et de déontologie extrêmement importante</li>
      <li>Avoir une très forte curiosité et une très large ouverture d’esprit</li>
      <li>Avoir une très grande culture générale</li>
      <li>Avoir un très grand sens de l’analyse et de la synthèse</li>
      <li>Avoir un bon esprit critique</li>
      <li>Avoir un projet de vie professionnel, où l’histoire dans ce projet sera un plus pour l’accomplir.</li>
    </ul>`
},{
  titre:"Et après…",
  text:`
    <h1 class="profile__content--title bleu">Le devenir des diplômé.e.s</h1>
    - Exemples de secteurs, de métiers, secteurs ou de poursuites d’études  possibles :<br>
    Masters dans le domaine du marketing, du journalisme, de l’enseignement et du patrimoine.<br>
    - Enquêtes sur les poursuites d’études après la licence d’Histoire et sur l’insertion des diplômé.es de master de l’Université Jean Monnet : <a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Statistiques et enquêtes de l’Université Jean Monnet</a><br>
    <a class="linkTxt bleu" href="http://enquetedhistoires.fr/#/sommaire/raconter">Témoignages des diplômé.e.s</a>`
},{
  titre: `L’offre de formation`,
  text:`
    <h1 class="profile__content--title bleu">L’offre de formation</h1>
    <ul>
      <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet</a></li>
      <li><a class="linkTxt bleu" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Faculté des Sciences Humaines et Sociales </a></li>
    </ul>`
}]
export  { fiche }
