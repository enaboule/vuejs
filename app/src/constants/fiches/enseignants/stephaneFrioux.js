const fiche =
[{
  titre:"Portrait",
  text:`
    <h1 class="profile__content--title bleu">Portrait</h1>
    Maître de conférences en Histoire contemporaine à l’Université Lumière Lyon 2.<br>
    Responsable du Master MEEF (métiers de l’enseignement, de l’éducation et de la formation) parcours Histoire-Géographie et de la préparation à l'agrégation.`
},{
  titre:"Compétences acquises",
  text:`
    <h1 class="profile__content--title bleu">Compétences acquises au cours des études d'histoire</h1>
    <ul>
      <li>être autonome dans son travail</li>
      <li>développer un regard critique sur les phénomènes sociaux et historiques</li>
      <li>savoir mener des recherches documentaires dans les bibliothèques et les salles d’archives</li>
      <li>savoir exposer des synthèses documentaires</li>
      <li>savoir rédiger et s’exprimer à l’oral</li>
    </ul>
    Les référentiels de compétences :
    <ul>
      <li><a class="linkTxt bleu" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a> >Ministère de l’Enseignement supérieur et de l’Innovation > Formations et diplômes > Licence </li>
      <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Le Référentiel de compétences de l’Université Jean Monnet</a></li>
    </ul>`
},{
  titre:"Quelques conseils pour réussir",
  text:`
    <h1 class="profile__content--title bleu">Quelques conseils pour réussir</h1>
    <ul>
      <li>goût d’apprendre</li>
      <li>intérêt pour toutes les périodes historiques</li>
      <li>capacités d’organisation notamment pour l’apprentissage</li>
      <li>envie de comprendre le système des sociétés, la vie politique, la vie culturelle</li>
      <li>intérêt pour l’actualité</li>
    </ul>`
},{
  titre:"Et après…",
  text:`
    <h1 class="profile__content--title bleu">Le devenir des diplômé.e.s</h1>
    <ul>
      - Exemples de secteurs, de métiers ou de poursuites d’études possibles :
      <li>métiers de l’enseignement</li>
      <li>journalisme</li>
      <li>tourisme : guide conférencier.e</li>
    </ul>
    - Le devenir des diplômé.e.s :<br>
    Enquêtes sur l'insertion des diplômé.e.s de master de l'université Lumière Lyon 2 : <a class="linkTxt bleu" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2</a>
    <a class="linkTxt bleu" href="http://enquetedhistoires.fr/#/sommaire/raconter">Témoignages des diplômé.e.s</a>`
},{
  titre: `L’offre de formation`,
  text:`
    <h1 class="profile__content--title bleu">L’offre de formation</h1>
    <ul>
      Université Lumière Lyon 2 :
      <li><a class="linkTxt bleu" href="http://offreformation.univ-lyon2.fr/cdm/">L’offre de formation</a></li>
      <li><a class="linkTxt bleu" href="http://ghhat.univ-lyon2.fr/">Le département Temps et Territoires</a></li>
    </ul>`
}]
export  { fiche }
