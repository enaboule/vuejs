const fiche =
[{
  titre:"Portrait",
  text:`
    <h1 class="profile__content--title bleu">Portrait</h1>
    Professeur des universités en Histoire médiévale à l’Université Lumière Lyon 2.`
},{
  titre:"Compétences acquises",
  text:`
    <h1 class="profile__content--title bleu">Compétences acquises au cours des études d'histoire</h1>
    <ul>
      <li>savoir synthétiser</li>
      <li>savoir s’exprimer à l’écrit et à l’oral</li>
      <li>comprendre et analyser les sociétés du passé de l’antiquité jusqu’à la période contemporaine</li>
      <li>développer une méthodologie pour la lecture critique des sources</li>
    </ul>
    Les référentiels de compétences :
    <ul>
      <li><a class="linkTxt bleu" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a> >Ministère de l’Enseignement supérieur et de l’Innovation > Formations et diplômes > Licence </li>
      <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Le Référentiel de compétences de l’Université Jean Monnet</a></li>
    </ul>`
},{
  titre:"Quelques conseils pour réussir",
  text:`
    <h1 class="profile__content--title bleu">Quelques conseils pour réussir</h1>
    <ul>
      <li>travail en bibliothèque et dans d’autres lieux dédiés à la culture</li>
      <li>autonomie et rigueur dans le travail</li>
      <li>maîtriser les outils d’expression</li>
    </ul>`
},{
  titre:"Et après…",
  text:`
    <h1 class="profile__content--title bleu">Le devenir des diplômé.e.s</h1>
    <ul>
      Exemples de secteurs, de métiers ou de poursuites d’études possibles :
      <li>secteur public ou privé</li>
      <li>enseignement</li>
      <li>recherche</li>
      <li>journalisme</li>
      <li>Organisations Non Gouvernementales (ONG)</li>
    </ul>
    - Le devenir des diplômé.e.s :<br>
    Enquêtes sur l'insertion des diplômé.e.s de master de l'université Lumière Lyon 2 : <a class="linkTxt bleu" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2</a>
    <a class="linkTxt bleu" href="http://enquetedhistoires.fr/#/sommaire/raconter">Témoignages des diplômé.e.s</a>`
},{
  titre: `L’offre de formation`,
  text:`
    <h1 class="profile__content--title bleu">L’offre de formation</h1>
    <ul>
      Université Lumière Lyon 2 :
      <li><a class="linkTxt bleu" href="http://offreformation.univ-lyon2.fr/cdm/">L’offre de formation</a></li>
      <li><a class="linkTxt bleu" href="http://ghhat.univ-lyon2.fr/">Le département Temps et Territoires</a></li>`
}]
export  { fiche }
