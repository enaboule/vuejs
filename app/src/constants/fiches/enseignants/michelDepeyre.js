const fiche =
[{
  titre:"Portrait",
  text:`
    <h1 class="profile__content--title bleu">Portrait</h1>
    Maître de conférences en Histoire Moderne et contemporaine à l’Université Jean Monnet Saint -Étienne`
},{
  titre:"Compétences acquises",
  text:`
    <h1 class="profile__content--title bleu">Compétences acquises au cours des études d'histoire</h1>
    <ul>
      <li>Analyser des faits historiques</li>
      <li>Savoir synthétiser</li>
      <li>Savoir rédiger</li>
    </ul>
    <li><a class="linkTxt bleu" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a> >Ministère de l’Enseignement supérieur et de l’Innovation > Formations et diplômes > Licence </li>
    <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Le Référentiel de compétences de l’Université Jean Monnet</a></li></ul>`
},{
  titre:"Quelques conseils pour réussir",
  text:`
    <h1 class="profile__content--title bleu">Quelques conseils pour réussir</h1>
    La première qualité valable pour toutes les études est la curiosité.<br>
    Il faut avoir envie de chercher, d’apprendre, de connaître, de comprendre l’autre dans le passé pour le comprendre dans le présent.`
},{
  titre:"Et après…",
  text:`
    <h1 class="profile__content--title bleu">Le devenir des diplômé.e.s</h1>
    - Exemples de secteurs, de métiers, secteurs ou de poursuites d’études possibles :
    <ul>
      <li>Enseignement</li>
      <li>Fonction publique</li>
      <li>Patrimoine</li>
      <li>Culture et musée</li>
    </ul>
    - Le devenir des diplômé.e.s :<br>
    <ul>
      <li>Enquêtes sur l'insertion des diplômé.e.s de master de l'université Lumière Lyon 2 : <a class="linkTxt bleu" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt bleu" href="http://enquetedhistoires.fr/#/sommaire/raconter">Témoignages des diplômé.e.s</a></li>
    </ul>`
},{
  titre: `L’offre de formation`,
  text:`
    <h1 class="profile__content--title bleu">L’offre de formation</h1>
    <ul>
      Les formations de l’université Jean Monnet :
      <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Université Jean Monnet</a></li>
      <li><a class="linkTxt bleu" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Faculté des Sciences Humaines et Sociales</a></li>
    </ul>`
}]
export  { fiche }
