const fiche =
[{
  titre:"Portrait",
  text:`
    <h1 class="profile__content--title bleu">Portrait</h1>
    Maîtresse de conférences en Histoire contemporaine à l’Université Lumière Lyon 2.`
},{
  titre:"Compétences acquises",
  text:`
    <h1 class="profile__content--title bleu">Compétences acquises au cours des études d'histoire</h1>
    <ul>
      <li>savoir analyser les documents</li>
      <li>savoir synthétiser</li>
      <li>savoir rédiger et s’exprimer à l’oral</li>
    </ul>
    Les référentiels de compétences :
    <ul>
      <li><a class="linkTxt bleu" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a> >Ministère de l’Enseignement supérieur et de l’Innovation > Formations et diplômes > Licence </li>
      <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Le Référentiel de compétences de l’Université Jean Monnet</a></li>
    </ul>`
},{
  titre:"Quelques conseils pour réussir",
  text:`
    <h1 class="profile__content--title bleu">Quelques conseils pour réussir</h1>
    <ul>
      <li>avoir de la curiosité</li>
      <li>aimer lire</li>
      <li>avoir le goût de la recherche d’informations dans les documents à partir des traces historiques</li>
      <li>suivre l’actualité en lien avec les questions contemporaines</li>
      <li>s’engager comme bénévole dans des associations, faire des stages</li>
    </ul>`
},{
  titre:"Et après…",
  text:`
    <h1 class="profile__content--title bleu">Le devenir des diplômé.e.s</h1>
    <ul>
      Exemples de secteurs, de métiers ou de poursuites d’études possibles :
      <li>enseignement</li>
      <li>journalisme</li>
      <li>médiation culturelle (musées, mémoriaux…)</li>
      <li>Organisation Non Gouvernementale (ONG)</li>
    </ul>
    - Le devenir des diplômé.e.s :<br>
    Enquêtes sur l'insertion des diplômé.e.s de master de l'université Lumière Lyon 2 : <a class="linkTxt bleu" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2</a>
    <a class="linkTxt bleu" href="http://enquetedhistoires.fr/#/sommaire/raconter">Témoignages des diplômé.e.s</a>`
},{
  titre: `L’offre de formation`,
  text:`
    <h1 class="profile__content--title bleu">L’offre de formation</h1>
    <ul>
      <li><a class="linkTxt bleu" href="http://offreformation.univ-lyon2.fr/cdm/">l'offre de formation de l'université Lumière Lyon 2</a></li>
      <li><a class="linkTxt bleu" href="http://ghhat.univ-lyon2.fr/">Le département Temps et Territoires</a></li>
    </ul>`
}]
export  { fiche }
