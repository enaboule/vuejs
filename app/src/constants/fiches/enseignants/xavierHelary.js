const fiche =
[{
  titre:"Portrait",
  text:`
    <h1 class="profile__content--title bleu">Portrait</h1>
    <ul>
      <li>Professeur des universités à l’Université Jean Moulin Lyon 3.</li>
      <li>Professeur d’Histoire médiévale, responsable du master Mondes médiévaux : Histoire, archéologie et littérature des mondes chrétiens et musulmans médiévaux.</li>
    </ul>`
},{
  titre:"Compétences acquises",
  text:`
    <h1 class="profile__content--title bleu">Compétences acquises au cours des études d'histoire</h1>
    <ul>
      <li>Connaitre toutes les périodes historiques</li>
      <li>Savoir rédiger des dissertations</li>
      <li>Savoir rédiger des commentaires de documents</li>
      <li>Développer l’esprit critique</li>
      <li>Être capable de dépouiller et d’analyser les sources</li>
    </ul>
    Les référentiels de compétences :
    <ul>
      <li><a class="linkTxt bleu" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a> >Ministère de l’Enseignement supérieur et de l’Innovation > Formations et diplômes > Licence </li>
      <li><a class="linkTxt bleu" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Le Référentiel de compétences de l’Université Jean Monnet</a></li>
    </ul>`
},{
  titre:"Quelques conseils pour réussir",
  text:`
    <h1 class="profile__content--title bleu">Quelques conseils pour réussir</h1>
    <ul>
      <li>Être passionné.e par l’Histoire</li>
      <li>Être apte aux travaux collectifs comme aux travaux individuels.</li>
    </ul>`
},{
  titre:"Et après…",
  text:`
    <h1 class="profile__content--title bleu">Le devenir des diplômé.e.s</h1>
    - Exemples de secteurs, de métiers ou de poursuites d’études possibles :
    <ul>
      <li>Enseignement –  Fonction publique</li>
      <li>Masters spécialisés : archives, patrimoine</li>
      <li>Passerelles vers les Instituts d’Etudes Politiques (IEP) ou les écoles de commerce</li>
    </ul>
    - Le devenir des diplômé.e.s :<br>
    <ul>
      <li>Enquêtes sur les poursuites d’études après la licence d’Histoire et sur l’insertion des diplômé.es de master de l’Université Jean Moulin Lyon 3 : <a class="linkTxt bleu" href="http://www.univ-lyon3.fr/fr/insertion-professionnelle/enquetes-d-insertion/les-enquetes-d-insertion-professionnelle-de-l-ofip-926114.kjsp">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt bleu" href="http://enquetedhistoires.fr/#/sommaire/raconter">Témoignages des diplômé.e.s</a></li>
    </ul>`
},{
  titre: `L’offre de formation`,
  text:`
    <h1 class="profile__content--title bleu">L’offre de formation</h1>
    <ul>
      <li><a class="linkTxt bleu" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt bleu" href="http://facdeslettres.univ-lyon3.fr/formations-histoire-787235.kjsp?RH=1396014493150">Département Histoire de l’Université Jean Moulin Lyon 3</a></li>
    </ul>`
}]
export  { fiche }
