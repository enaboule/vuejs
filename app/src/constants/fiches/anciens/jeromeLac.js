const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
      <p>Conseiller principal d'éducation dans un lycée public</p>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire, université Jean Monnet Saint-Étienne</li>
      <li>Master mention Systèmes, Territoires, Environnements, Patrimoines, université Jean Monnet Saint-Étienne</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <ul>
      <li>Surveillant d’externat</li>
      <li>Assistant d’éducation</li>
    </ul>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <p>
      Le/la Conseiller.ère Principal.e d’Éducation est le/la responsable de la vie scolaire hors de la classe. Il/elle prend en compte les absences, les retards. Il/elle est à l’écoute des élèves en situation de mal être, en difficultés scolaires ou personnelles.<br>
      Il/elle est le lien entre les familles, les enseignant.es, l’équipe éducative et la direction de l’établissement.
    </p>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <ul>
      <li>Licence Histoire + master Métiers de l'enseignement, de l'éducation et de la formation –MEEF - Encadrement éducatif + Certificat d’Aptitude aux fonctions de Conseiller Principal d’Education (CACPE)</li>
      <li>Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a> Les Écoles supérieures du professorat et de l’éducation - ÉSPÉ </li>
      <li><a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr/pid33962/les-espe-pour-former-les-futurs-enseignants.html">Les Écoles supérieures du professorat et de l’éducation – ÉSPÉ</a></li>
      <li>L' Ecole supérieure du professorat et de l’éducation, académie de Lyon - ÉSPÉ Lyon1 : <a class="linkTxt jaunetest" href="http://www.univ-lyon1.fr">www.univ-lyon1.fr</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Autres appellations du métier</h1>
    <p>CPE</p>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>
        CIDJ - <a class="linkTxt jaunetest" href="http://www.cidj.com">www.cidj.com</a> Fiche métier - Conseiller / Conseillère principal(e) d'éducation - CPE<br>
        Les métiers de l’enseignement
      </li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a> Fiche métier - Conseiller/ère principal/e d'éducation</li>
      <li>
        Répertoire Opérationnel des Métiers et des Emplois - ROME<br>
        Pôle Emploi : www.pole-emploi.fr Fiche ROME - K2104 - Éducation et surveillance au sein d'établissements d'enseignement
      </li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Organiser la vie des élèves dans l’établissement, en dehors des cours</li>
      <li>Gérer les conflits entre élèves</li>
      <li>Prendre en compte les absences et les retards des élèves</li>
      <li>Être à l’écoute et accompagner les élèves dans leur parcours</li>
      <li>Faire le lien entre les élèves, les parents, l’équipe éducative et de direction de l’établissement</li>
    </ul>
    <p>Le référentiel de compétences des métiers du professorat et de l’éducation <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a></p>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <ul>
      <li>Les établissements scolaires du 2nd degré, en collège, et en lycée</li>
      <li>
        Ministère de l'éducation nationale - <a class="linkTxt jaunetest" href="http://www.education.gouv.fr">www.education.gouv.fr</a><br>
        Concours  Conseillère principal(e) d'éducation - CPE
      </li>
      <li>
        Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a><br>
        Concours Conseillère principal(e) d'éducation - CPE
      </li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
