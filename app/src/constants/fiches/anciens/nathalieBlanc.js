const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <p>Professeure documentaliste en lycée</p>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence et maîtrise en Histoire, université Lumière Lyon 2</li>
      <li>Master Culture de l’écrit et de l’image, université Lumière Lyon 2</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <ul>
      <li>Médiatrice culturelle saisonnière dans des musées privés, territoriaux et nationaux</li>
      <li>Gestionnaire financière dans des entreprises privées</li>
      <li>Bibliothécaire en bibliothèque municipale et universitaire</li>
    </ul>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
      <ul>
        <li>Le/la professeur.e documentaliste est  le/la responsable du fonds documentaire, il/elle en assure la gestion et sa mise en valeur</li>
        <li>Il/elle accueille le public. Il fait de la médiation culturelle</li>
        <li>Il/elle met en valeur la lecture publique pour attirer les jeunes vers la culture, les livres afin de motiver leur curiosité</li>
        <li>Il/elle travaille en partenariat avec les enseignant.es pour monter des séquences pédagogiques</li>
      </ul>
      <h1 class="profile__content--title jaune">Autres appellations du métier</h1>
      <ul>
        <li>Enseignant.e documentaliste</li>
        <li>Documentaliste en CDI ou en établissement scolaire</li>
      </ul>
      <h1 class="profile__content--title jaune">Formations possibles</h1>
      <ul>
        <li>Licence Histoire + master Métiers de l’Enseignement de l’Éducation et de la Formation 2nd degré</li>
        <li>MEEF de la spécialité + Certificat d’Aptitude au Professorat de l’Enseignement du Second degré (CAPES) section documentation</li>
      </ul>
      <ul>
        <li>Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a> Les Écoles supérieures du professorat et de l’éducation - ÉSPÉ</li>
        <li>L' Ecole supérieure du professorat et de l’éducation, académie de Lyon - ÉSPÉ Lyon1 : <a class="linkTxt jaunetest" href="http://www.univ-lyon1.fr">www.univ-lyon1.fr</a></li>
      </ul>
      <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
      <ul>
        <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
        <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
        <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
      </ul>
      <h1 class="profile__content--title jaune">Webographie</h1>
      <ul>
        <li>CIDJ - <a class="linkTxt jaunetest" href="http://www.cidj.com">www.cidj.com</a> Fiche métier - Professeur-documentaliste</li>
        <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.www.onisep.fr">www.onisep.fr</a> Fiche métier - Professeur/e documentaliste</li>
        <li>
          Répertoire Opérationnel des Métiers et des Emplois - ROME
          Pôle Emploi : <a class="linkTxt jaunetest" href="http://www.pole-emploi.fr">www.pole-emploi.fr</a> Fiche ROME - K1601 - Gestion de l'information et de la documentation
        </li>
      </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Accueillir le public</li>
      <li>Gérer et mettre en valeur le fonds documentaire</li>
      <li>Attirer les jeunes vers la culture, les livres et motiver leur curiosité</li>
      <li>Travailler en partenariat avec les enseignant.es pour monter des séquences pédagogiques</li>
    </ul>
    <p>Le référentiel de compétences des métiers du professorat et de l’éducation Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a></p>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <ul>
      <li>
        Ministère de l'éducation nationale - <a class="linkTxt jaunetest" href="http://www.education.gouv.fr">www.education.gouv.fr</a>
        Concours Professeur/e documentaliste
      </li>
      <li>
        Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a><br>
        Concours Professeur/e documentaliste
      </li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
