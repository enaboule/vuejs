const fiche =
[{
  titre:"Son Histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <p>Chargé de développement, Communication et médiation au Mémorial national de la prison de Montluc</p>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire, université Jean Moulin Lyon 3</li>
      <li>Master Métier du patrimoine, université Jean Moulin Lyon 3</li>
      <li>Master University College of Cork - Irlande</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <ul>
      <li>Stagiaire puis chercheur consultant – UNESCO – Division du dialogue interculturel</li>
      <li>Chargé de développement – Musée Tour du Moulin, Bourgogne</li>
      <li>Chargé de développement – Musée du Teil, Ardèche</li>
    </ul>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <p>Connaître la méthode de la recherche historique</p>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <ul>
      <li>Effectuer des recherches historiques et archivistiques</li>
      <li>Réaliser des synthèses</li>
      <li>Participer à des publications et des expositions</li>
      <li>Gérer le développement du mémorial, sa programmation culturelle (événements, conférences, expositions)</li>
      <li>Communiquer autour de ces événements auprès des publics</li>
    </ul>
    <h1 class="profile__content--title jaune">Autres appellations du métier</h1>
    <ul>
      <li>Chargé.e de l’action culturelle</li>
      <li>Animateur/trice culturel.le</li>
      <li>Chargé.e de développement culturel</li>
    </ul>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <ul>
      <li>DUT Gestion administrative et commerciale des organisations (GACO) option Arts</li>
      <li>Licence professionnelle Gestion de projets artistiques et culturels</li>
      <li>Master Médiation culturelle, gestion culturelle</li>
    </ul>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>CIDJ - <a class="linkTxt jaunetest" href="http://www.cidj.com">www.cidj.com</a> Fiche métier - Médiateur culturel / Médiatrice culturelle Les métiers du management culturel</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a> Fiche métier - Responsable de projets culturels</li>
      <li>
        Répertoire interministériel des métiers de l'État - RIME <br>
        Portail de la fonction publique <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a> Fiche FPECUL04 - Chargée / chargé du développement des publics et de l'action culturelle dans un établissement patrimonial
      </li>
      <li>
        Répertoire des métiers de la fonction publique territoriale <br>
        Centre national de la fonction publique territoriale - CNFPT : <a class="linkTxt jaunetest" href="http://www.cnfpt.fr">www.cnfpt.fr</a> Fiche 04D32 -Médiatrice / Médiateur culturel-le
      </li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Capacité de transmission du savoir</li>
      <li>Créativité</li>
      <li>Curiosité intellectuelle</li>
      <li>Sens de l’organisation</li>
      <li>Aisance à l’oral</li>
      <li>Sens des relations humaines</li>
      <li>Connaitre les périodes historiques</li>
      <li>Appliquer les méthodes de recherches historiques</li>
      <li>Avoir une bonne connaissance des sources historiques</li>
    </ul>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <p>Les musées, les médiathèques, les associations, les centres culturels. Selon le type de structure, le statut du/de la salarié.e peut être de droit privé ou relever de la fonction publique d’État ou territoriale.</p>
    <a class="linkTxt jaunetest" href="http://www.memorial-montluc.fr/">Mémorial national de la prison Montluc</a>
    <h1 class="profile__content--title jaune">Les associations professionnelles</h1>
    <ul>
      <li>Association de bénévoles de la Médiation culturelle : <a class="linkTxt jaunetest" href="http://www.mediationculturelle.net">www.mediationculturelle.net</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Témoignages des employeur.euse.s</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Isabelle-rive-dore">Mme DORE  RIVE : Directrice CHRD de Lyon</a></li>
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Xavier-De-La-Selle">M. Xavier DE LA SELLE : Directeur des musées de Lyon</a></li>
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Yvan-Perreton">M. Yvan PERRETON : Musée du chapeau</a></li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
