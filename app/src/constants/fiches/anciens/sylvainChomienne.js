const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <ul>
      <li>Bibliothécaire assistant spécialisé (Bibas) au Service Commun de la Documentation (SCD) d’Aix-Marseille Universités. Emploi de catégorie B, personnel des bibliothèques</li>
      <li>Responsable des collections Lettres et Sciences humaines et sociales</li>
    </ul>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire</li>
      <li>DUT gestion de l’information et du document, université Pierre Mendès-France Grenoble</li>
      <li>Master recherche en Histoire contemporaine, université Lumière Lyon 2</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <p>Stages puis contrat à durée déterminée (CDD) en Médiathèque en charge de l’acquisition et muséographie</p>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <ul>
      <li>Ouverture d’esprit</li>
      <li>Rigueur</li>
    </ul>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <ul>
      <li>Le/la bibliothécaire gère le fonds documentaire et le met à disposition du public.</li>
      <li>Il/elle est le responsable d’une ou plusieurs collections</li>
      <li>Il/elle fait l’acquisition d’ouvrages dans certaines disciplines</li>
      <li>Le/la bibliothécaire est en lien avec les enseignant.es et les étudiant.es.</li>
    </ul>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <ul>
      <li>DUT</li>
      <li>Diplôme d'études universitaires scientifiques et techniques (DEUST)</li>
      <li>Licence</li>
      <li>Licence professionnelle Métiers du livre</li>
      <li>Master Métiers du livre et de l'édition</li>
    </ul>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>CIDJ - <a class="linkTxt jaunetest" href="http://www.cidj.com">www.cidj.com</a> Fiche métier - Bibliothécaire</li>
      <li>Les métiers des bibliothèques</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a> Fiche métier -  Bibliothécaire</li>
      <li>
        Répertoire Opérationnel des Métiers et des Emplois - ROME <br>
        Pôle Emploi : www.pole-emploi.fr Fiche ROME - K1601 - Gestion de l'information et de la documentation
      </li>
      <li>
        Répertoire des métiers de la fonction publique territoriale <br>
        Centre national de la fonction publique territoriale - CNFPT : <a class="linkTxt jaunetest" href="http://www.cnfpt.fr">www.cnfpt.fr</a> Fiche Bibliothécaire
      </li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Accueillir le public</li>
      <li>Mettre à disposition des documents</li>
      <li>Traiter et conserver des collections</li>
      <li>Acquérir des ouvrages dans certaines disciplines (Lettres et Sciences humaines et sociales.)</li>
      <li>Être en lien avec les enseignant.es et les étudiant.es.</li>
    </ul>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <ul>
      <li>Bibliothèques universitaires, municipales, départementales ou nationales</li>
      <li>
        La fonction publique d'État<br>
        Ministère de l'enseignement supérieur de la recherche et de l'innovation : <a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a>
      </li>
      <li>
        Centre national de la fonction publique territoriale - <br>
        CNFPT : <a class="linkTxt jaunetest" href="http://www.cnfpt.fr">www.cnfpt.fr</a>
      </li>
    </ul>
    <ul>
      Témoignages des employeur.euse.s
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Laurence-Khamkham">Laurence Khamkham, Médiat Rhône-Alpes</a></li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
