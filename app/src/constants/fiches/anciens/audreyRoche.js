const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <ul>
      <li>Responsable des collections archéologiques du musée savoisien de Chambéry</li>
      <li>Attachée de conservation du patrimoine, spécialité archéologie</li>
      <li>Emploi de catégorie A de la filière culturelle de la fonction publique territoriale</li>
    </ul>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire, université Jean Moulin Lyon 3</li>
      <li>Maîtrise Histoire de l’art et archéologie, université Clermont Ferrand</li>
      <li>Master Archéologie et histoire des mondes anciens. Archéologie et patrimoine, option archéologie, université Jean Moulin Lyon 3</li>
      <li>Préparation au concours de conservateur du patrimoine – spécialité archéologie, école du Louvre</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <ul>
      <li>Chantiers de fouilles archéologiques (contrats de vacations)</li>
      <li>Plusieurs postes dans la fonction publique territoriale</li>
    </ul>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <p>Les connaissances en archéologie sont utilisées au quotidien</p>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <p>
      Le/la chef.fe de projet scientifique d’un musée départemental a la responsabilité des collections archéologiques du musée<br>
      Il/elle en garantit la conservation pour les générations futures
    </p>
    <h1 class="profile__content--title jaune">Il/elle a en charge</h1>
    <ul>
      <li>La classification</li>
      <li>La conservation</li>
      <li>L’étude et la valorisation des objets du musée</li>
      <li>Il/elle doit aussi communiquer (par l’organisation de colloques, de rencontres) et organiser le prêt des objets à d’autres musées</li>
      <li>Il/elle est en contact quotidien avec des objets historiques.</li>
    </ul>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <p>Master Archéologie, voire doctorat</p>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3 </a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>Les métiers du management culturel</li>
      <li>Les études d’Histoire de l’art</li>
      <li>Travailler dans la fonction publique territoriale</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a> Fiche métier - Conservateur/trice du patrimoine</li>
      <li>
        Répertoire Opérationnel des Métiers et des Emplois - ROME<br>
        Pôle Emploi : www.pole-emploi.fr Fiche ROME - K1062 - Gestion du patrimoine culturel
      </li>
      <li>
        Répertoire interministériel des métiers de l'État - RIME<br>
        Portail de la fonction publique : <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.onisep.fr</a> Fiche FPE2CUL08 - Chargée / chargé de collections ou de fonds patrimoniaux
      </li>
      <li>
        Répertoire des métiers de la fonction publique territoriale<br>
        Centre national de la fonction publique territoriale - CNFPT : <a class="linkTxt jaunetest" href="http://www.cnfpt.fr">www.cnfpt.fr</a> Fiche 02D32 - Archéologue
      </li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Connaitre de manière approfondies les thématiques du fonds du musée (antiquité, archéologie, histoire de l’art).</li>
      <li>Connaitre les langues anciennes</li>
      <li>Connaître des régions de fouilles archéologiques</li>
      <li>Maîtriser les règles de gestion administrative</li>
      <li>Assurer la gestion d’une équipe</li>
      <li>Savoir communiquer</li>
      <li>Être méticuleux/euse</li>
      <li>Être autonome</li>
    </ul>
    <a class="linkTxt jaunetest" href="http://www.culturecommunication.gouv.fr/Nous-connaitre/Emploi-et-formation/Le-repertoire-des-metiers">Répertoire des métiers de la communication et de la culture</a>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <p>L’État, les collectivités territoriales et les établissements publics qui leur sont rattachés</p>
    <ul>
      <li>Ministère de la culture : <a class="linkTxt jaunetest" href="http://www.culturecommunication.gouv.fr">www.culturecommunication.gouv.fr</a> Archéologie > Chantiers de bénévoles sur territoire national</li>
      <li><a class="linkTxt jaunetest" href="http://www.musee-savoisien.fr/">Musée savoisien, musée départemental d’histoire et de culture de la Savoie</a></li>
    </ul>
    <h1 class="profile__content--title jaune">En savoir plus sur les concours d’accès aux emplois</h1>
    <p>Le concours d’attaché.e territorial.e du patrimoine spécialité archéologie :</p>
    <ul>
      <li>
        Le/la candidat.e au concours doit choisir l’une des 5 spécialités proposées
        <ul>
          <li>Archéologie</li>
          <li>Archives</li>
          <li>Inventaire</li>
          <li>Musée</li>
          <li>Patrimoine scientifique technique et naturel</li>
        </ul>
      </li>
      <li>Le concours externe est ouvert aux titulaires d’un diplôme bac +3</li>
      <li>Les concours sont organisés par les centres de gestion</li>
      <li>
        Le/la lauréat.e du concours est affecté.e dans un emploi en lien avec la spécialité choisie :
        <ul>
          <li>Archéologue</li>
          <li>Archiviste</li>
          <li>Régisseur/euse d’œuvres</li>
          <li>Restaurateur/trice d’œuvres</li>
          <li>Directeur/trice d’établissement patrimonial</li>
        </ul>
      </li>
    </ul>
    <h1 class="profile__content--title jaune">Exemples de préparations au concours</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.cned.fr/inscription/6XM75DIX">CNED : Attaché.e territorial.e de conservation du patrimoine</a></li>
      <li>Institut national du patrimoine - INP : <a class="linkTxt jaunetest" href="http://www.inp.fr">www.inp.fr</a> Les préparations aux concours</li>
      <li>École du Louvre : www.ecoledulouvre.fr Classes préparatoire aux concours de conservateur du patrimoine</li>
      <li>
        Participer en tant que bénévoles à des chantiers de fouilles:
        Ministère de la culture : <a class="linkTxt jaunetest" href="http://www.culturecommunication.gouv.fr">www.culturecommunication.gouv.fr</a> Archéologie > Chantiers de bénévoles sur territoire national
      </li>
      <li>Institut national de recherche archéologique préventive - INRAP : <a class="linkTxt jaunetest" href="http://www.inrap.fr">www.inrap.fr</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Témoignages des employeur.euse.s</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Isabelle-rive-dore">Mme DORE  RIVE : Directrice CHRD de Lyon</a></li>
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Xavier-De-La-Selle">M. Xavier DE LA SELLE : Directeur des musées de Lyon</a></li>
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Yvan-Perreton">M. Yvan PERRETON : Musée du chapeau</a></li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
