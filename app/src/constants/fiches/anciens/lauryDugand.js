const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <p>Généalogiste à l’Étude Guénifey</p>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire</li>
      <li>Master 1 recherche en Histoire</li>
      <li>master 2 professionnel « Patrimoine rural, valorisation culturelle »</li>
      <li>université Lumière Lyon 2</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <ul>
      <li>médiatrice culturelle dans le cadre d’un Service civique</li>
      <li>commissaire d’exposition pour la conception et réalisation d’expositions à la Bibliothèque municipale de Lyon</li>
    </ul>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <p>capacité à exploiter les sources historiques, c’est-à-dire la recherche et l’utilisation des données recueillies</p>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <ul>
      <li>Le/la généalogiste recherche des héritier/ères dans le cadre de successions</li>
      <li>Il/elle fait l’arbre généalogique complet à la suite d’un décès et retrace l’historique de la famille</li>
      <li>Le/la généalogiste est amené.e à se déplacer régulièrement en France ou à l’étranger dans les services d’archives, dans les tribunaux, les mairies… </li>
    </ul>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <ul>
      <li>Licence ou master en Droit ou en Histoire pour travailler dans le service recherche d’une Étude</li>
      <li>Une formation complémentaire est dispensée en interne par les Cabinets de généalogie</li>
    </ul>
    <h1 class="profile__content--title jaune">Autres appellations du métier</h1>
    <p>généalogiste successoral.e</p>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <p>Le métier de généalogiste. <a class="linkTxt jaunetest" href="https://www.orientation-education.com/metier/genealogiste">Le métier de généalogiste</a></p>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Rigueur et discrétion sont indispensables pour l’exercice de la profession</li>
      <li>Être autonome dans le travail</li>
      <li>Très bonne connaissance des sources ainsi que de bonnes connaissances de l’histoire du pays ou de la région liés à la recherche</li>
    </ul>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <ul>
      <li>Entreprises du secteur privé</li>
      <li>
        Associations professionnelles :
        <ul>
          <li>Association des généalogistes de France <a class="linkTxt jaunetest" href="http://www.genealogistes-France.org">www.genealogistes-France.org</a></li>
        </ul>
      </li>
    </ul>
    <ul>
      Témoignages des employeur.euse.s
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Genevieve-Michelin">Étude Guénifey (Mme Michelin)</a></li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
