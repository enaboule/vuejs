const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <ul>
      <li>Assistant territorial qualifié de conservation du patrimoine et des bibliothèques</li>
      <li>Emploi de catégorie B de la filière culturelle de la fonction publique territoriale</li>
    </ul>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire et master Histoire contemporaine</li>
      <li>Université Lumière Lyon 2</li>
      <li>Master Archives, université Jean Moulin Lyon 3</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <ul>
      <li>Stage dans le cadre du master</li>
      <li>Premier emploi en tant qu’agent contractuel grâce au réseau de l’association des anciens du master Archives</li>
    </ul>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <ul>
      <li>Développement de l’esprit de curiosité</li>
      <li>Acquisition d’une méthodologie de travail</li>
    </ul>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <p>L’archiviste a pour missions principales de collecter, conserver, classer et mettre les documents à la disposition du public.</p>
    <h1 class="profile__content--title jaune">Autres appellations du métier</h1>
    <ul>
      <li>archiviste adjoint.e</li>
      <li>archiviste-documentaliste</li>
    </ul>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <ul>
      <li>DUT</li>
      <li>Information-communication option « métiers du livre et du patrimoine »</li>
      <li>Licence Histoire</li>
      <li>licence professionnelle métiers de l’information-archives</li>
      <li>métiers de l’édition</li>
      <li>master Archives</li>
      <li>Archives numériques</li>
    </ul>
    École nationale des Chartes formation des archivistes <a class="linkTxt jaunetest" href="http://www.enc-sorbonne.fr/">www.enc-sorbonne.fr</a> Formation des archivistes
    École Nationale Supérieure des Sciences de l’Information et des Bibliothèques (ENSSIB). <a class="linkTxt jaunetest" href="http://www.enssib.fr">www.enssib.fr</a>
    <ul>
      <li>
        <h4>Les formations en Histoire des universités</h4>
        <ul>
          <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
          <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
          <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
        </ul>
      </li>
      <li>CIDJ - <a class="linkTxt jaunetest" href="https://www.cidj.com/">www.cidj.com</a> Fiche métier - Archiviste</li>
      <h4>Webographie</h4>
      <li>ONISEP - <a class="linkTxt jaunetest" href="https://www.onisep.fr">www.onisep.fr</a> Fiche métier - Archiviste </li>
      <li>Répertoire Opérationnel des Métiers et des Emplois -ROME Pôle Emploi : www.pole-emploi.fr Fiche ROME K1601 - Gestion de l'information et de la documentation</li>
      <li>Répertoire interministériel des métiers de l'Etat - RIME  Portail de la fonction publique : <a class="linkTxt jaunetest" href="https://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a> Fiche FP2CUL01 -Responsable du développement des publics et de l'action culturelle</li>
      <li>Répertoire des métiers de la fonction publique territoriale Centre national de la fonction publique territoriale - CNFPT : <a class="linkTxt jaunetest" href="https://www.cnfpt.fr">www.cnfpt.fr</a> Fiche 03/D/32  - Archiviste </li>
      <li>Répertoire des métiers de la communication et de la culture Ministère de la culture : <a class="linkTxt jaunetest" href="https://www.culturecommunication.gouv.fr">www.culturecommunication.gouv.fr</a></li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>apprécier le contact avec le public</li>
      <li>s’adapter à des demandes de recherches différentes selon les publics</li>
      <li>être passionné.e par l’Histoire</li>
      <li>avoir un esprit de synthèse</li>
      <li>être curieux/euse</li>
      <li>encadrer du personnel</li>
    </ul>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    On peut travailler dans un service d’archives d’une administration ou dans un établissement d’archives (Archives nationales, départementales, municipales) ou dans le secteur privé.<br>
    <ul><li><a class="linkTxt jaunetest" href="http://www.archives-lyon.fr">Archives municipales de Lyon</a></li></ul>
    <h1 class="profile__content--title jaune">En savoir plus sur les concours d’accès aux emplois</h1>
    Assistant.e territorial.e qualifié.e de conservation du patrimoine et des bibliothèques :
    Le concours est ouvert aux titulaires d’un diplôme équivalent à un diplôme bac + 2. Le/la candidat.e doit choisir l’une des 4 spécialités proposées : musée, bibliothèque, archives, documentation. Le/la lauréat.e au concours est affecté.e dans un emploi en lien avec la spécialité choisie.<br>
    Les concours sont organisés par les centres de gestion.
    <ul>
      <li>Les concours et métiers de la fonction publique de l’État. Portail de la fonction publique : <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Associations professionnelles</h1>
    <ul><li>Association des archivistes français. <a class="linkTxt jaunetest" href="http://www.archivistes.org">www.archivistes.org</a></li></ul>
    <h1 class="profile__content--title jaune">Témoignages des employeur.euse.s</h1>
    <ul><li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Isabelle-rive-dore">M.Galland : directeur des archives départementales du Rhône</a></li></ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
