const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <p>Responsable du service culturel des Bibliothèques universitaires de l’université Jean Moulin Lyon 3</p>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire, université de Grenoble</li>
      <li>Master Patrimoine et archéologie, université Jean Moulin Lyon 3</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <ul>
      <li>Stage de 6 mois au château de Fontainebleau</li>
      <li>Directeur adjoint d’une bibliothèque patrimoniale à Epinal en contrat à durée déterminée</li>
    </ul>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <ul>
      <li>Développer l’esprit de curiosité</li>
      <li>Acquérir une bonne méthode de travail</li>
    </ul>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <p>
      Piloter des projets de nature culturelle nécessite de rencontrer de nombreux interlocuteur/trices en fonction des projets en cours
      Un.e chef.fe de projet culturel est à l’initiative des nouveaux projets d’expositions, de conférences, de journées d’étude
    </p>
    <h1 class="profile__content--title jaune">Autres appellations du métier</h1>
    <ul>
      <li>responsable de projets culturels</li>
      <li>chef.fe de projets culturels</li>
      <li>organisateur/trice d’événements</li>
    </ul>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <ul>
      <li>DUT Gestion administrative et commerciale des organisations GACO option Arts (bac+2)</li>
      <li>licence professionnelle, master spécialisés en médiation culturelle, gestion culturelle</li>
    </ul>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>CIDJ - <a class="linkTxt jaunetest" href="http://www.cidj.com">www.cidj.com</a> Fiche métier - Médiateur culturel / Médiatrice culturelle</li>
      <li>Les métiers du management culturel</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a> Fiche métier - Responsables de projets culturels</li>
      <li>
        Répertoire interministériel des métiers de l'État -RIME
        Portail de la fonction publique <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a> Fiche FP2EPP04 - Responsable projet
      </li>
      <li>
        Répertoire des métiers de la fonction publique territoriale
        Centre national de la fonction publique territoriale - CNFPT : <a class="linkTxt jaunetest" href="http://www.cnfpt.fr">www.cnfpt.fr</a> Fiche 04D32 -Médiatrice / Médiateur culturel-le Fiche 02D28 - Cheffe / Chef de projet culturel
      </li>
    </ul>
    <p>Répertoire des métiers de la communication et de la culture Ministère de la culture : <a class="linkTxt jaunetest" href="http://www.culturecommunication.gouv.fr">www.culturecommunication.gouv.fr</a></p>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Capacités de communication</li>
      <li>qualités relationnelles</li>
      <li>Capacité d’organisation</li>
      <li>Curiosité intellectuelle</li>
      <li>Créativité</li>
      <li>Travail en réseau</li>
      <li>Gestion d’équipe</li>
      <li>Une méthode de travail et une capacité d’analyse acquises lors de l’étude des faits historiques</li>
    </ul>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <ul>
      <li>Les musées</li>
      <li>les bibliothèques</li>
      <li>les médiathèques</li>
      <li>les associations</li>
      <li>les centres culturels</li>
    </ul>
    <p>Selon les cas, le statut peut être de droit privé ou relever de la fonction publique d’État ou territoriale.</p>
    <a class="linkTxt jaunetest" href="http://bu.univ-lyon3.fr/">Bibliothèques universitaires de l’université Jean Moulin Lyon 3</a>
    <h1 class="profile__content--title jaune">Les associations professionnelles</h1>
    <ul>
      <li>Association de bénévoles de la Médiation culturelle : <a class="linkTxt jaunetest" href="http://www.mediationculturelle.net">www.mediationculturelle.net</a></li>
    </ul>
    <ul>
      Témoignages des employeur.euse.s
      <li><a class="linkTxt jaunetest" href="http://enquetedhistoires.fr/#/sommaire/conseiller/Laurence-Khamkham">Laurence Khamkham, Médiat Rhône-Alpes</a></li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
