const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <p>Professeur d’Histoire-Géographie dans un lycée d’enseignement général privé. Titulaire du CAFEP, Certificat d’Aptitude aux Fonctions d’Enseignement dans les établissements Privés sous contrat du 2nd degré.</p>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire, université Jean Moulin Lyon 3</li>
      <li>Master 1 Recherche Histoire contemporaine, université Jean Moulin Lyon 3</li>
      <li>Master 2 Enseignement, université Jean Moulin Lyon 3</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <p>Plusieurs stages en établissements scolaires</p>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <p>Un bon équilibre entre savoirs et méthode</p>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <p>
      Le/la professeur/eure de collège et de lycée enseigne à la fois l’Histoire et la Géographie, qu’il/elle soit spécialiste de l’une ou de l’autre discipline au départ.<br>
      Il y a plusieurs facettes du métier : <br>faire le cours devant les élèves (18h par semaine) ; préparer les cours ; corriger les copies (évaluation des élèves) ; participer à des réunions d’équipes ou disciplinaires.<br>
      D’autres enseignements complémentaires sont possibles : <br>Enseignement Moral et Civique (EMC), Travaux Personnels Encadrés (TPE), Enseignements Pratiques Interdisciplinaires (EPI) au collège, enseignements d’exploration en classe de seconde, enseignement de l’Histoire en langues étrangères (avec une certification complémentaire).
    </p>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <p>Licence Histoire + master Métiers de l’Enseignement de l’Éducation et de la Formation 2nd degré - MEEF - de la spécialité + CAPES ou CAFEP ou agrégation (bac+5 minimum)</p>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3 </a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>CIDJ - <a class="linkTxt jaunetest" href="http://www.cidj.com">www.cidj.com</a><br> Fiche métier - Professeur de lycée et collège</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a><br>Fiche métier - Professeur/e de collège et de lycée</li>
      <li>Répertoire Opérationnel des Métiers et des Emplois - ROME<br>
        Pôle Emploi : www.pole-emploi.fr<br>
        Fiche ROME - K2107 - Enseignement général du second degré</li>
      <li>Répertoire interministériel des métiers de l'État -RIME<br>
        Portail de la fonction publique <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a><br>
        Fiche FPE2EDU02 - Enseignant / enseignante</li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Connaissances approfondies des périodes historiques et du programme scolaire de collège et lycée</li>
      <li>Maîtrise de la méthode de la dissertation et du commentaire de document</li>
      <li>Capacité d’adaptation</li>
      <li>Aisance à l’oral</li>
      <li>Organisation</li>
      <li>Rigueur</li>
      <li>Autorité naturelle</li>
    </ul>
    <p>Le référentiel de compétences des métiers du professorat et de l’éducation Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a></p>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <p>
      Les établissements scolaires du 2nd degré, collèges et lycées.<br>
      Ministère de l’Education nationale <a class="linkTxt jaunetest" href="http://www.education.gouv.fr/">www.education.gouv.fr/</a>
    </p>
    <h1 class="profile__content--title jaune">En savoir plus sur les concours d’accès aux emplois</h1>
    <p>Le CAFEP spécialité Histoire et Géographie :</p>
    <ul>
      <li>Le Certificat d’Aptitude aux Fonctions d’Enseignement dans les établissements Privés – CAFEP -  permet d’enseigner dans les collèges et lycées privés sous contrat avec l’État. Les épreuves du concours sont identiques à celles du CAPES. Les titulaires du CAFEP doivent postuler auprès d’un établissement et ils ont un statut de contractuels même si leur rémunération est prise en charge par l’État.</li>
      <li>L' Ecole supérieure du professorat et de l’éducation, académie de Lyon - ÉSPÉ Lyon1 : <a class="linkTxt jaunetest" href="http://www.univ-lyon1.fr">www.univ-lyon1.fr/</a></li>
      <li>
        Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr/</a><br>
        Les Écoles supérieures du professorat et de l’éducation - ÉSPÉ
      </li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
