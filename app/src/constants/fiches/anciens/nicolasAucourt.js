const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <ul>
      <li>Chargé de projets : politique de la ville, renouvellement urbain et citoyenneté au Secrétariat général pour les affaires régionales, services de la Préfecture de Région-Lyon</li>
      <li>Attaché principal d’administration de l’État</li>
      <li>Emploi de catégorie A de la fonction publique d’État</li>
    </ul>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire</li>
      <li>Université Lumière Lyon 2</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <p>Ministère de l’équipement à Paris puis à Lyon</p>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <ul>
      <li>méthodologie pour préparer et réussir des concours administratifs</li>
      <li>capacités de synthèse</li>
      <li>savoir faire des recherches documentaires</li>
      <li>maîtrise de l’expression écrite et orale</li>
    </ul>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <ul>
      <li>conduire des projets en lien avec les politiques publiques urbaines en collaboration avec les Directions départementales du territoire, le Conseil régional</li>
      <li>animer un réseau et piloter des réunions avec des partenaires extérieur.es, services départementaux</li>
      <li>préparer des dossiers pour le Préfet de région en vue de la tenue de réunions</li>
    </ul>
    <h1 class="profile__content--title jaune">Autres appellations du métier</h1>
    <ul>
      <li>chargé.e de mission de renouvellement urbain</li>
      <li>responsable de projets</li>
      <li>chargé.e de développement territorial</li>
    </ul>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <p>Licence en Histoire, Droit, Administration publique, le plus souvent. Une préparation au concours d’entrée aux Instituts Régionaux d’Administration (IRA) est conseillée.</p>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Les formations en Histoire des universités</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>CIDJ - <a class="linkTxt jaunetest" href="https://www.cidj.com/">www.cidj.com</a> Fiche métier - Attaché territorial </li>
      <li>Les métiers de la fonction publique</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="https://www.onisep.fr">www.onisep.fr</a> Fiche métier - Travailler dans la fonction publique</li>
      <li>
        Répertoire interministériel des métiers de l'Etat -RIME- <br>
        Portail de la fonction publique : <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a><br>
        Fiche FP2EPP04 - Responsable de projet<br>
        Fiche FP2EPP11 - Délégué du Préfet pour la politique de la ville
      </li>
      <li>
        Répertoire des métiers de la fonction publique territoriale <br>
        Centre national de la fonction publique territoriale - CNFPT : <a class="linkTxt jaunetest" href="http://www.cnfpt.fr">www.cnfpt.fr</a> <br>
        Fiche 05/A/01 - Chargé.e d'évaluation des politiques publiques <br>
        Fiche 07/B/08 - Chargé.e du développement territorial
      </li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>connaissance approfondie du fonctionnement des collectivités territoriales</li>
      <li>concevoir des projets d’aménagement et/ou piloter des études urbaines</li>
      <li>connaître les procédures réglementaires et financières</li>
      <li>coordonner les différents acteurs des collectivités</li>
      <li>élaborer des documents de travail et/ou de synthèse</li>
      <li>s’adapter à son environnement de travail en faisant le lien avec les disciplines étudiées comme l’histoire urbaine</li>
      <li>connaissances approfondies en géographie et cartographie lorsque le travail est en lien avec le domaine de l’urbanisme</li>
    </ul>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <p>L’État et les établissements publics qui leur sont rattachés. Emplois de fonctionnaires titulaires ou de personnels contractuels.</p>
    <h1 class="profile__content--title jaune">En savoir plus sur les concours d’accès aux emplois</h1>
    <ul>
      <li>Emplois de catégorie A de la fonction publique d’État.</li>
      <li>Les IRA forment pendant 18 mois les futur.e.s attaché.e.s d’administration qui exerceront dans les ministères ou les administrations. A la fin de leur formation les élèves émettent des vœux pour leur choix d’affectation sur un poste. Il existe en France 4 autres instituts du même type à Bastia, Lille, Metz et Nantes.</li>
      <li>On peut préparer le concours d’entrée aux IRA avec le Centre de préparation à l’administration générale: (CPAG) Sciences Po Lyon <a class="linkTxt jaunetest" href="http://www.sciencespo-lyon.fr">www.sciencespo-lyon.fr</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.ira-lyon.gouv.fr">l’IRA de Lyon</a></li>
      <li>Le Portail de la fonction publique  <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a>. Rubrique Les métiers les agents les rémunérations</li>
      <li>Instituts Régionaux d'Administration. IRA de Lyon <a class="linkTxt jaunetest" href="http://www.ira-lyon.gouv.fr/fr/Présentation/Directeur.aspx">www.ira-lyon.gouv.fr</a></li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
