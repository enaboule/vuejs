const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    Directeur d’une agence spécialisée dans l’événementiel cinéma- Cinésium
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire, université Lumière Lyon 2</li>
      <li>DEA Histoire ancienne, université Lumière Lyon 2</li>
      <li>Titulaire du CAPES Histoire-Géographie et de l’agrégation Histoire</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <ul>
      <li>Professeur d’Histoire-Géographie en collège et en lycée</li>
      <li>Organisation de conférences autour des thèmes Histoire et cinéma à l’Institut Lumière (Lyon)</li>
    </ul>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <ul>
      <li>Une bonne culture générale</li>
      <li>de l’aisance dans les échanges</li>
      <li>ouverture d’esprit</li>
    </ul>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <p>
      Organisation de conférences, de festivals associant histoire et cinéma : élaborer un plan de communication événementiel, un descriptif précis de l’événement, sa contextualisation ; rechercher des intervenant.es, définir un budget, des moyens, des outils de communication.
      Actions de formations auprès d’entreprises. Outre le diplôme, les expériences dans le secteur culturel sont très importantes dans ce métier.
    </p>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <ul>
      <li>Master en Histoire spécialisé communication culturelle</li>
      <li>Master en communication</li>
      <li>Licence professionnelle « communication événementielle »</li>
    </ul>
    <h1 class="profile__content--title jaune">Autres appellations du métier</h1>
    <ul>
      <li>Chargé.e de communication événementielle</li>
      <li>chef.fe de projet événementiel</li>
      <li>coordinateur/trice d’événements</li>
    </ul>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>Les métiers de la communication</li>
      <li>Les métiers du management culturel</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a> Fiche métier : Chargé.e de projet événementiel</li>
      <li>
        Répertoire Opérationnel des Métiers et des Emplois - ROME
        Pôle Emploi : www.pole-emploi.fr Fiche ROME - E1107 - Organisation d'événementiel
      </li>
      <li>
        Répertoire interministériel des métiers de l'État -RIME
        Portail de la fonction publique <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a> Fiche FPECOM04 - Chargé de communication événementielle
      </li>
      <li>
        Répertoire des métiers de la communication et de la culture
        Ministère de la culture : <a class="linkTxt jaunetest" href="http://www.culturecommunication.gouv.fr">www.culturecommunication.gouv.fr</a>
      </li>
    </ul>
    <p><a class="linkTxt jaunetest" href="http://metiersdelacommunication.fr/">Le référentiel des métiers de la communication</a></p>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Aisance relationnelle</li>
      <li>Sens de la diplomatie</li>
      <li>Capacité d’organisation, coordination, planification d’événements</li>
      <li>Aptitude à l’animation de conférences</li>
      <li>Capacité d’adaptation</li>
      <li>Polyvalence technique</li>
      <li>Développer un réseau professionnel</li>
      <li>Bonne culture générale</li>
      <li>Appliquer la méthode de la recherche historique</li>
    </ul>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <ul>
      <li>Les agences d’événementiel</li>
      <li>de communication</li>
      <li>les sociétés de services</li>
    </ul>
    <ul>
      Agence événementielle spécialisée dans le cinéma
      <li><a class="linkTxt jaunetest" href="http://www.cinesium.fr/">Cinésium</a></li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
