const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <p>Doctorant chargé de cours à l’université Paris Est Marne la Vallée</p>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence d’Histoire et licence de Géographie, université Jean Moulin Lyon 3</li>
      <li>Master Histoire spécialité enseignement, <br> université Jean Moulin Lyon 3</li>
      <li>Master recherche Histoire moderne et contemporaine, université Paris-Est</li>
      <li>Titulaire du CAPES et de l’agrégation d’Histoire</li>
      <li>Thèse de doctorat en cours de préparation</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <p>Professeur dans le secondaire</p>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <ul>
      <li>capacité à mener des recherches documentaires sur le terrain en archives ou en bibliothèques</li>
      <li>développer l’esprit de curiosité</li>
      <li>connaissances approfondies des disciplines Histoire et Géographie</li>
    </ul>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <p>
      Le/la doctorant.e effectue des recherches en Histoire en vue de la préparation de sa thèse tout en étant en lien avec d’autres enseignant.es associé.es à des travaux de recherche précis dans un laboratoire (avec le CNRS, par exemple).<br>
      Il/elle assure une activité d’enseignement auprès d’étudiant.es de 1re année de licence en tant que chargé.e de cours en travaux dirigés (TD).<br>
      Autres appellations du métier : lorsqu’on devient Enseignant.e-Chercheur//euse, il existe deux statuts, maître//esse de conférences et professeur//eure des universités.
    </p>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <ul>
      <li>Master et thèse de doctorat</li>
      <li>concours de l’agrégation et expérience de l’enseignement</li>
      <li>La thèse de doctorat est préparée en 3 ans après le master soit 8 ans d’études après le bac</li>
      <li>Les candidat.es se positionnent sur une liste de postes ouverts sur la France et après avoir obtenu l’aval du Conseil National des universités. Sélection sur dossier et entretien.</li>
    </ul>
    <a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/pid24530/les-enseignants-chercheurs.html">Les étudiant.e.s inscrit.e.s en 1re année de thèse sous contrat doctoral</a>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>CIDJ - <a class="linkTxt jaunetest" href="https://www.cidj.com/">www.cidj.com</a> Fiche métier - Enseignant-chercheur</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a> Fiche métier - Enseignant.e - Chercheur//euse</li>
      <li>
        Répertoire Opérationnel des Métiers et des Emplois -ROME
        Pôle Emploi : www.pole-emploi.fr Fiche ROME - K2108 Enseignement supérieu
      </li>
      <li>
        Répertoire interministériel des métiers de l'Etat -RIME
        Portail de la fonction publique : <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a> Fiche FP2RCH02 - Enseignant.e - Chercheur//euse
      </li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
  <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Aimer enseigner et transmettre des connaissances aux étudiant.es</li>
      <li>Être autonome dans le travail</li>
      <li>Savoir mener un travail de recherche</li>
      <li>Savoir identifier et exploiter les sources, dans les services d’archives, les bibliothèques, pour la préparation de la thèse.</li>
    </ul>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <ul>
      <li>Les établissements d’enseignement supérieur : universités, grandes écoles, classes préparatoires, IUT</li>
    </ul>
    <ul>
      Les enseignante.s chercheurs//euses sont soit :
      <li>des enseignant.e.s fonctionnaires titulaires  de la fonction publique d’État (Ministère de l’enseignement supérieur et de l’innovation ou Ministère de l’éducation nationale)</li>
      <li>des enseignant.e.s  contractuel.les. Ils/elles doivent postuler auprès des services ressources humaines des établissements concernés</li>
    </ul>
    <h1 class="profile__content--title jaune">En savoir plus sur les concours d’accès aux emplois</h1>
    <p>Ministère de l'enseignement supérieur de la recherche et de l'innovation : <a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr">www.enseignementsup-recherche.gouv.fr</a> Enseignants-chercheurs</p>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1><ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>

    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>

    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>

    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`

  }]

export  { fiche }
