const fiche =
[{
  titre:"Son histoire",
  text:`
    <h1 class="profile__content--title jaune">Poste</h1>
    <ul>
      <li>Professeure de Lettres-Histoire et géographie dans lycée professionnel à Andrézieux-Bouthéon</li>
      <li>Titulaire du Certificat d'Aptitude au Professorat de Lycée Professionnel (CAPLP)</li>
    </ul>
    <h1 class="profile__content--title jaune">Diplômes</h1>
    <ul>
      <li>Licence Histoire, université Jean Monnet Saint-Étienne</li>
      <li>Master mention Systèmes, Territoires, Environnements, Patrimoines, université Jean Monnet Saint-Étienne</li>
    </ul>
    <h1 class="profile__content--title jaune">Expériences</h1>
    <p>Assistante d’éducation comme maître/sse d'internat et externat</p>
    <h1 class="profile__content--title jaune">Apport des études d’Histoire</h1>
    <p>Apprendre à faire des cherches documentaire dans les livres d’histoire mis à disposition en bibliothèque universitaire</p>`
},{
  titre:"Son métier",
  text:`
    <h1 class="profile__content--title jaune">Nature du travail</h1>
    <p>Le/la professeur.e en lycée professionnel enseigne à la fois le français et l’histoire géographie.</p>
    <h1 class="profile__content--title jaune">Son temps de travail est partagé entre :</h1>
    <ul>
      <li>Préparer les cours</li>
      <li>Assurer les cours aux élèves (18h par semaine)</li>
      <li>Corriger les copies (évaluation des élèves)</li>
    </ul>
    <p>Il/Elle enseigne aussi l’Enseignement Moral et Civique (EMC)</p>
    <h1 class="profile__content--title jaune">Formations possibles</h1>
    <p>Licence Histoire + MASTER Métiers de l’Enseignement de l’Éducation et de la Formation – (MEEF) second degré de la spécialité et Concours d'Accès au corps des Professeurs de Lycée Professionnel (CAPLP)</p>
    <ul>
      <li>Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a> Les Écoles supérieures du professorat et de l’éducation - ÉSPÉ </li>
      <li>L' Ecole supérieure du professorat et de l’éducation, académie de Lyon - ÉSPÉ Lyon1 : <a class="linkTxt jaunetest" href="http://www.univ-lyon1.fr">www.univ-lyon1.fr</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Les formations en Histoire des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://ghhat.univ-lyon2.fr/">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://facdeslettres.univ-lyon3.fr/departement-histoire-816093.kjsp?RH=LET-ACCUEIL_FR">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://fac-shs.univ-st-etienne.fr/fr/index.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title jaune">Webographie</h1>
    <ul>
      <li>CIDJ - <a class="linkTxt jaunetest" href="http://www.cidj.com">www.cidj.com</a> Fiche métier - Professeur de lycée et collège</li>
      <li>Les métiers de l’enseignement</li>
      <li>Professeur de lycée et collège CAPLP</li>
      <li>ONISEP - <a class="linkTxt jaunetest" href="http://www.onisep.fr">www.onisep.fr</a> Fiche métier - Professeur/e de collège et de lycée</li>
      <li>
        Répertoire Opérationnel des Métiers et des Emplois - ROME<br>
        Pôle Emploi : www.pole-emploi.fr Fiche ROME - K2107- Enseignement général du second degré
      </li>
      <li>
        Répertoire interministériel des métiers de l'État - RIME<br>
        Portail de la fonction publique : <a class="linkTxt jaunetest" href="http://www.fonction-publique.gouv.fr">www.fonction-publique.gouv.fr</a> Fiche FPE2EDU02 - ENSEIGNANTE/ENSEIGNANT
      </li>
    </ul>`
},{
  titre:"Les compétences",
  text:`
    <h1 class="profile__content--title jaune">Exemples de compétences utiles dans l’exercice du métier</h1>
    <ul>
      <li>Rechercher des documents</li>
      <li>Concevoir et préparer ses cours</li>
      <li>Réaliser des synthèses de documents</li>
      <li>Avoir une aisance orale</li>
    </ul>
    <p>Le référentiel de compétences des métiers du professorat et de l’éducation Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a></p>`
},{
  titre:"Les employeur/euses",
  text:`
    <h1 class="profile__content--title jaune">Types d’employeur/euses</h1>
    <p>Les établissements scolaires du 2nd degré, les lycées professionnels.</p>
    <a class="linkTxt jaunetest" href="http://www.education.gouv.fr/">Ministère de l’Education nationale</a>
    <h1 class="profile__content--title jaune">En savoir plus sur les concours d’accès aux emplois</h1>
    <p>Le CAPLP spécialité lettres histoire et géographie :</p>
    <ul>
      <li>Le Certificat d'Aptitude au Professorat de Lycée Professionnel – CAPLP- permet d’enseigner dans les lycées professionnels publics.</li>
      <li>
        Ministère de l'éducation nationale - <a class="linkTxt jaunetest" href="http://www.education.gouv.fr">www.education.gouv.fr</a><br>
        Concours professeur/e de collège et de lycée
      </li>
      <li>
        Devenir enseignant : <a class="linkTxt jaunetest" href="http://www.devenirenseignant.gouv.fr">www.devenirenseignant.gouv.fr</a><br>
        Concours professeur/e de collège et de lycée
      </li>
    </ul>`
},{
  titre: `Les ressources`,
  text:`
    <h1 class="profile__content--title">L’offre de formation des universités</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://offreformation.univ-lyon2.fr/cdm/">Les formations de l’université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/les-formations-de-l-universite-jean-moulin-499123.kjsp?RH=1305190882406&RF=INS-FORMglob">Les formations de l’université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/catalogue-des-formations.html">Les formations de l’université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les référentiels de compétences</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/_attachments/rubrique-orientation-insertion-professionnelle-article/Guide%2520licence%25202015.pdf?download=true">Université Jean Monnet Saint-Étienne : Le référentiel de compétences</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.enseignementsup-recherche.gouv.fr/cid61532/www.enseignementsup">Les référentiels  de compétences des mentions de licence</a></li>
    </ul>
    <h1 class="profile__content--title">Les enquêtes d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon2.fr/universite/les-chiffres/">Université Lumière Lyon 2 : Le devenir des diplômé.es en Histoire : rubrique « Les chiffres ».</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/le-devenir-de-nos-diplomes-481237.kjsp?RH=1290596423423&RF=1290596423423">Université Jean Moulin Lyon 3 : Devenir des diplômé.es : Suivi des inscrits en Licence 3 et enquêtes d’insertion professionnelle des diplômé.es de master – faculté des Lettres et Civilisations</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/dpaiq/statistiques-et-enquetes.html">Université Jean Monnet Saint-Étienne :Statistiques et enquêtes</a></li>
    </ul>
    <h1 class="profile__content--title">Les Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://etu.univ-lyon2.fr/orientation-stage/presentation-du-scuio-ip-381831.kjsp?RH=ETU-Rub8">Université Lumière Lyon 2</a></li>
      <li><a class="linkTxt jaunetest" href="http://www.univ-lyon3.fr/orientation-et-insertion-professionnelle-875810.kjsp?RH=INS-VIEEinfo&RF=INS-VIEEinfo">Université Jean Moulin Lyon 3</a></li>
      <li><a class="linkTxt jaunetest" href="https://www.univ-st-etienne.fr/fr/formation/orientation-insertion.html">Université Jean Monnet Saint-Étienne</a></li>
    </ul>
    <h1 class="profile__content--title">Les ressources documentaires des Services d’information d’orientation et d’insertion professionnelle</h1>
    <ul>
      <li><a class="linkTxt jaunetest" href="http://catalogue.univ-lyon2.fr/">Université Lumière Lyon 2 : Catalogue des ouvrages et dossiers du SCUIO-IP</a></li>
      <li><a class="linkTxt jaunetest" href="http://pmb.univ-lyon3.fr/opac_css/">Université Jean Moulin Lyon 3 : Base de données documentaire du SCUIO-IP</a></li>
    </ul>`
}]
export  { fiche }
