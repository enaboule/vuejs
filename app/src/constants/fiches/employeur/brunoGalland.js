const fiche =
[{
  titre:"L’employeur.euse",
  text:`
    <h1 class="profile__content--title rouge">L’Employeur.euse</h1>
    Bruno GALLAND : Directeur des Archives du département du Rhône et de la métropole de Lyon<br>
    Rattachées au Conseil départemental du Rhône, les Archives départementales ont pour mission de conserver les fonds des administrations publiques du Rhône, les archives notariales et certaines archives d'origine privée.<br>
    Parmi les 60 personnes employées,  environ 15 personnes ont une formation en histoire.<br>
    Les Archives départementales du Rhône et de la métropole de Lyon : <a class="linkTxt rouge" href="http://archives.rhone.fr/">www.archives.rhone.fr</a>`
},{
  titre:"Les Compétences attendues",
  text:`
    <h1 class="profile__content--title rouge">Les Compétences attendues</h1>
    <p>Pour réussir les concours : </p>
    <ul>
      <li>Avoir les connaissances historiques pour comprendre les documents d’archives</li>
      <li>Être méthodique et rigoureux</li>
    </ul>`
},{
  titre:"Le Recrutement",
  text:`
    <h1 class="profile__content--title rouge">Le Recrutement</h1>
    <ul>
      <li>Un master “Archives” est requis, soit directement après une licence Histoire, soit en complément d’un premier master Histoire.</li>
      <li>L’accès au métier est possible par la voie des concours de la fonction publique territoriale (emploi dans les communes, régions, départements)  ou  par contrat à durée déterminée.</li>
    </ul>
    <ul>
      Les concours de catégorie A :
      <li>Centre National de la Fonction Publique Territoriale (CNFPT) : <a class="linkTxt rouge" href="http://www.cnfpt.fr/">www.cnfpt.fr</a></li>
      <li>Fédération Nationale des Centres de Gestion (FNCDG) et Centres de gestion</li>
      <liInstitut National du Patrimoine (INP) <a class="linkTxt rouge" href="http://www.inp.fr/">www.inp.fr</a></li>
    </ul>
    <ul>
      Autres concours :
      <li>assistant.e qualifié.e</li>
      <li>assistant.e et assistant.e territorial.e du patrimoine (concours de catégorie B)</li>
    </ul>
    Consulter le témoignage de Tristan Vuillet, archiviste : <a class="linkTxt rouge" href="http://enquetedhistoires.fr/#/sommaire/raconter/Tristan-Vuillet">Tristan VUILLET</a>
    <ul>
      Le diplôme requis varie selon la catégorie du concours concerné :
      <li>Concours de catégorie A : licence minimum exigée, un diplôme bac + 5 est requis pour certains concours.</li>
      <li>Concours de catégorie B : baccalauréat minimum exigé, un diplôme bac +2 est requis pour certains concours.</li>
    </ul>
     Le site du Portail de la fonction publique : <a class="linkTxt rouge" href="https://www.fonction-publique.gouv.fr/score/concours/conditions-propres-aux-concours-externes">www.fonction-publique.gouv.fr</a><br>
     <a class="linkTxt rouge" href="http://enquetedhistoires.fr/#/sommaire/raconter/Tristan-Vuillet">Tristan VUILLET</a><br>`
}]
export  { fiche }
