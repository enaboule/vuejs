const fiche =
[{
  titre:"L’employeur.euse",
  text:`
    <h1 class="profile__content--title rouge">L’Employeur.euse</h1>
    Claire Moyrand directrice du pôle parisien de l’entreprise <br>«Histoire d’Entreprises». <br>
    L’entreprise a deux pôles, un parisien et un lyonnais.<br>
    Histoire d’Entreprises est une agence spécialisée dans la valorisation du patrimoine historique technique, culturel et humain des entreprises et marques.<br>
    L’entreprise emploie une dizaine d’employé.es.<br>
    <a class="linkTxt rouge" href="http://www.histoire-entreprises.fr/"> Histoire d’entreprises</a>`
},{
  titre:"Les Compétences attendues",
  text:`
    <h1 class="profile__content--title rouge">Les Compétences attendues</h1>
    <p>Pour réussir les concours : </p>
    <ul>
      <li>Avoir une grande rigueur</li>
      <li>Être méthodique</li>
      <li>Avoir une grande capacité d'analyse et de synthèse</li>
      <li>Être curieux</li>
    </ul>`
},{
  titre:"Le Recrutement",
  text:`
    <h1 class="profile__content--title rouge">Le Recrutement</h1>
    Les personnes recrutées ont toutes un double cursus, un master en Histoire et une formation complémentaire.`
}]
export  { fiche }
