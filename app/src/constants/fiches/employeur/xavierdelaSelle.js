const fiche =
[{
  titre:"L’employeur.euse",
  text:`
    <h1 class="profile__content--title rouge">L’Employeur.euse</h1>
    <p>Directeur des musées Gadagne, coordonnateur du musée de l'imprimerie et de la communication et du musée Henri Malartre (Lyon et Rochetaillée-sur-Saône)</p>
    <p>
      Les musées Gadagne : <a class="linkTxt rouge" href="http://www.gadagne.musees.lyon.fr/">www.gadagne.musees.lyon.fr</a><br>
      Le musée de l’imprimerie et de la communication : <a class="linkTxt rouge" href="http://www.imprimerie.lyon.fr/imprimerie/">www.imprimerie.lyon.fr/imprimerie</a><br>
      Le musée Henri Malartre : <a class="linkTxt rouge" href="http://www.musee-malartre.com/malartre/">www.musee-malartre.com/malartre</a><br>
    </p>
    <p>consulter la vidéo de Mme Despierres, Directrice du musée, à la suite de l'interview de M.de la Selle.</p>`
},{
  titre:"Les Compétences attendues",
  text:`
    <h1 class="profile__content--title rouge">Les Compétences attendues</h1>
    <p>pour réussir les concours :</p>
    <ul>
      <li>avoir des connaissances historiques</li>
      <li>savoir mener des recherches</li>
      <li>savoir faire partager le goût de l’histoire et les connaissances sur les thèmes des musées</li>
      <li>intérêt pour le travail avec le public</li>
      <li>rigueur intellectuelle</li>
    </ul>`
},{
  titre:"Le Recrutement",
  text:`
    <h1 class="profile__content--title rouge">Le Recrutement</h1><p>
        Niveau de recrutement en général : licence ou master en Histoire.<br>
        Les personnels doivent réussir un concours de la fonction publique territoriale (emploi dans les communes, régions, départements)<br>
        Répertoire des métiers de la fonction publique territoriale,CNFPT : <a class="linkTxt rouge" href="http://www.cnfpt.fr/">www.cnfpt.fr</a><br>
        Les employé.es font partie des personnels de la Ville de Lyon. Les musées peuvent aussi recruter des personnes en contrat à durée déterminée pour des missions ponctuelles.<br>
        Exemples de postes occupés :  médiateurs/trices culturel.les, chargé.es de collection, chargé.es de documentation.
      </p>
      <p>
        <ul>
          Le diplôme requis varie selon la catégorie du concours concerné :
          <li>Concours de catégorie A : licence minimum exigée, un diplôme bac + 5 est requis pour certains concours.</li>
          <li>Concours de catégorie B : baccalauréat minimum exigé, un diplôme bac +2 est requis pour certains concours.</li>
        </ul>
        Le site du Portail de la fonction publique :<a class="linkTxt rouge" href="https://www.fonction-publique.gouv.fr/score/concours/conditions-propres-aux-concours-externes">www.fonction-publique.gouv.fr</a><br>
      </p>`

  }]

export  { fiche }
