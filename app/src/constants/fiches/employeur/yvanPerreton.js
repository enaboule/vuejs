const fiche =
[{
  titre:"L’employeur.euse",
  text:`
    <h1 class="profile__content--title rouge">L’Employeur.euse</h1>
    Directeur adjoint à l’atelier du musée du Chapeau à Chazelles-sur-Lyon (structure culturelle, touristique avec une dimension économique).<br>
    Chargé de la communication sur l’ensemble de la structure envers le grand public, le centre de formation et en interne.<br>
    <a class="linkTxt rouge" href="http://www.museeduchapeau.com/">Musée du chapeau</a>`
},{
  titre:"Les Compétences attendues",
  text:`
    <h1 class="profile__content--title rouge">Les Compétences attendues</h1>
    <p>Pour réussir les concours : </p>
    <ul>
      <li>Savoir rédiger</li>
      <li>Savoir analyser</li>
      <li>Savoir synthétiser</li>
      <li>Savoir prendre en charge un groupe de personnes</li>
      <li>Savoir captiver et parler avec passion de son domaine</li>
    </ul>`
},{
  titre:"Le Recrutement",
  text:`
    <h1 class="profile__content--title rouge">Le Recrutement</h1>
    Les salarié.es permanent.es sont diplômé.es en Histoire.<br>
    Des étudiant.es en cours d’études en licence ou en master d’histoire sont employé.es comme contractuel.les. Ils/elles occupent souvent des postes de guide de musée.`
}]
export  { fiche }
