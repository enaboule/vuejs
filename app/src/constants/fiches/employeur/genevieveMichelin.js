const fiche =
[{
  titre:"L’employeur.euse",
  text:`
    <h1 class="profile__content--title rouge">L’Employeur.euse</h1>
    Directrice régionale de l’Etude généalogique Guénifey à Lyon<br>
    La société est spécialisée dans la recherche d’héritier.es pour les successions<br>
    8 collaborateurs/rices sont employé.es dans la succursale lyonnaise dont 4 diplômé.es en Histoire pour des postes de « chercheurs/euses »<br>
    <a class="linkTxt rouge" href="http://www.etudeguenifey.com">Etude généalogique Guénifey</a>`
},{
  titre:"Les Compétences attendues",
  text:`
    <h1 class="profile__content--title rouge">Les Compétences attendues</h1>
    <p>Pour réussir les concours : </p>
    <ul>
      <li>aptitude à mener des recherches dans les services d’archives</li>
      <li>savoir constituer des dossiers administratifs et juridiques</li>
      <li>faire preuve d’une grande rigueur et de qualités d’organisation, discrétion</li>
      <li>être autonome dans le travail</li>
      <li>maîtriser une langue étrangère</li>
      <li>posséder le sens du relationnel (contact avec des héritier.es retrouvé.es)</li>
      <li>être disponible pour de nombreux déplacements en France et à l’étranger</li>
    </ul>`
},{
  titre:"Le Recrutement",
  text:`
    <h1 class="profile__content--title rouge">Le Recrutement</h1>
    <p>
      On recrute en général avec un master en Histoire et si possible une 1ere expérience professionnelle. Le recrutement se fait par annonces dans la presse puis une formation est assurée en interne autour des méthodes de recherche en généalogie.<br>
      Consulter le témoignage de Laury Dugand, chercheuse généalogiste : <a class="linkTxt rouge" href="http://enquetedhistoires.fr/#/sommaire/raconter/Laury-Dugand">Laury Dugand</a>
    </p>`
}]

export  { fiche }
