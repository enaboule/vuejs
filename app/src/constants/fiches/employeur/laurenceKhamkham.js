const fiche =
[{
  titre:"L’employeur.euse",
  text:`
    <h1 class="profile__content--title rouge">L’Employeur.euse</h1>
    <p>
      Responsable pédagogique de Médiat Rhône-Alpes (site de Lyon)<br>
      Responsable de la préparation aux concours d’accès aux métiers des bibliothèques de catégorie A (bibliothécaire et conservateur/trice des bibliothèques).<br>
      Médiat Rhône-Alpes : centre régional de formation aux carrières des bibliothèques (CRFCB) localisé sur deux sites, Lyon et Grenoble.<br>
      Prépare aux épreuves écrites et orales des concours des bibliothèques de la fonction publique d’Etat et de la fonction publique territoriale (emploi dans les communes, régions, départements).<br>
      Assure le perfectionnement, la formation continue des professionnel.les de la documentation et des bibliothèques.<br>
      <a class="linkTxt rouge" href="http://mediat.univ-grenoble-alpes.fr/">Médiat Rhône-Alpes</a>
    </p>`
},{
  titre:"Les Compétences attendues",
  text:`
    <h1 class="profile__content--title rouge">Les Compétences attendues</h1>
    <p>Pour réussir les concours : </p>
    <ul>
      <li>avoir l’esprit de synthèse</li>
      <li>avoir l’esprit d’analyse</li>
      <li>avoir des qualités rédactionnelles.</li>
    </ul>`
},{
  titre:"Le Recrutement",
  text:`
    <h1 class="profile__content--title rouge">Le Recrutement</h1>
    Le niveau master est fortement conseillé pour réussir les concours d’accès aux métiers cadres (catégorie A) : bibliothécaire et conservateur/trice des bibliothèques.<br>
    Exemples de postes occupés en bibliothèques : responsables des services aux publics, de l’action culturelle, de la communication, des services aux chercheurs.<br>
    <ul>
      Les concours de catégorie A
      <li>Centre National de la Fonction Publique Territoriale (CNFPT)<a class="linkTxt rouge" href="http://www.cnfpt.fr/">www.cnfpt.fr</a></li>
      <li>Bibliothécaire territorial.e : Fédération Nationale des Centres de Gestion (FNCDG) et sites des Centre De gestion</li>
      <li>Ministère de l'enseignement supérieur, rubrique Personnels des bibliothèques : <a class="linkTxt rouge" href="http://www.enseignementsup-recherche.gouv.fr/pid24792/personnels-des-bibliotheques.html">www.enseignementsup-recherche.gouv.fr</a></li>
    </ul>
    <ul>
      Autres concours :
      <li>bibliothécaire assistant.e spécialisé.e (BibAS) (concours de catégorie B)</li>
    </ul><br>
    Consulter le témoignage de Sylvain Chomienne, bibliothécaire : <a class="linkTxt rouge" href="http://enquetedhistoires.fr/#/sommaire/raconter/Sylvain-Chomienne">Sylvain Chomienne</a><br>
    <p>
      <ul>
        Le diplôme requis varie selon la catégorie du concours concerné :
        <li>Concours de catégorie A : licence minimum exigée, un diplôme bac + 5 est requis pour certains concours.</li>
        <li>Concours de catégorie B : baccalauréat minimum exigé, un diplôme bac +2 est requis pour certains concours.</li>
      </ul>
      <a class="linkTxt rouge" href="https://www.fonction-publique.gouv.fr/score/concours/conditions-propres-aux-concours-externes">Fonction Publique</a>
    </p>`
}]
export  { fiche }
