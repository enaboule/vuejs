const fiche =
[{
  titre:"L’employeur.euse",
  text:`
    <h1 class="profile__content--title rouge">L’Employeur.euse</h1>
    Isabelle Doré-Rivé directrice du Centre d’Histoire de la Résistance et la Déportation à Lyon.<br>
    25 collaborateurs sont employés, c’est un des plus importants musées consacré à la Seconde Guerre mondiale en France.<br>
    <a class="linkTxt rouge" href="http://www.chrd.lyon.fr/chrd/">Centre d’Histoire de la Résistance et de la Déportation</a>`
},{
  titre:"Les Compétences attendues",
  text:`
    <h1 class="profile__content--title rouge">Les Compétences attendues</h1>
    <p>Pour réussir les concours : </p>
    <ul>
      <li>Analyser les documents conservés au musée</li>
      <li>Mettre ces documents dans le contexte et les rendre accessibles au plus grand nombre</li>
    </ul>`
},{
  titre:"Le Recrutement",
  text:`
    <h1 class="profile__content--title rouge">Le Recrutement</h1>
    Les salariés ont généralement une licence ou un master. Théoriquement le recrutement se fait par la  voie de concours de catégorie B et A  de la fonction publique territoriale. Les contractuels sont employés en contrat à durée déterminée.<br>
    <p>
      <ul>
        Le diplôme requis varie selon la catégorie du concours concerné :
        <li>Concours de catégorie A : licence minimum exigée, un diplôme bac + 5 est requis pour certains concours.</li>
        <li>Concours de catégorie B : baccalauréat minimum exigé, un diplôme bac +2 est requis pour certains concours.</li>
      </ul>
      <a class="linkTxt rouge" href="https://www.fonction-publique.gouv.fr/score/concours/conditions-propres-aux-concours-externes">Fonction Publique</a>
    </p>`
  }]
export  { fiche }
