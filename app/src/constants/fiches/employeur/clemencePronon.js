const fiche =
[{
  titre:"L’employeur.euse",
  text:`
    <h1 class="profile__content--title rouge">L’Employeur.euse</h1>
    Cofondatrice de l’entreprise Cybèle avec Olivier Montillet.<br>
    Cybèle propose à l’année une vingtaine de visites insolites dans toute la ville de Lyon.<br>
    3 personnes sont employées à temps plein et d’autres sont employées ponctuellement pour préparer ou mener des visites.<br>
    Entreprise Cybèle : <a class="linkTxt rouge" href="https://www.cybele-arts.fr">www.cybele-arts.fr</a>`
},{
  titre:"Les Compétences attendues",
  text:`
    <h1 class="profile__content--title rouge">Les Compétences attendues</h1>
    <p>Pour réussir les concours : </p>
    <ul>
      <li>être historien.ne</li>
      <li>être capable de mener des visites guidées</li>
      <li>savoir mener des recherches documentées pour la construction de nouvelles visites guidées</li>
      <li>être polyvalent quand on travaille dans une petite structure</li>
      <li>aimer le théâtre et jouer des rôles</li>
    </ul>`
},{
  titre:"Le Recrutement",
  text:`
    <h1 class="profile__content--title rouge">Le Recrutement</h1>
    Pas de niveau ou de diplôme pré requis mais des connaissances en histoire sont nécessaires pour construire des visites historiques.`
}]
export  { fiche }
